﻿using System;
using System.Collections.Generic;
using System.Configuration;
using ThirdAndQuoteInterface.Model;
using Objets100cLib;
using ThirdAndQuoteInterface.Utils;

namespace ThirdAndQuoteInterface.BusinessLogic
{
    class AccountProcess
    {
        public static void CreateAccounts(List<Account> accounts, BSCIALApplication100c businessManagementDb)
        {
            foreach (var account in accounts)
            {
                if (AccountsExists(businessManagementDb, account))
                {
                    Console.WriteLine($"client deja existant num : {account.StudentAccount}");
                }
                else
                {
                    try
                    {
                        CreateAccount(businessManagementDb, account);
                        Console.WriteLine($"tiers créer  num : {account.StudentAccount}");
                    }
                    catch (Exception e)
                    {
                        //TODO LOG
                        Console.WriteLine($"Message : {e.Message} - Trace : {e.StackTrace}   num : {account.StudentAccount}");
                    }
                }
            }
        }

        private static void CreateAccount(BSCIALApplication100c businessManagementDb, Account account)
        {
            var newAccount = (IBOClient3)businessManagementDb.CptaApplication.FactoryClient.Create();

            newAccount.CT_Num = StringUtils.CheckLength(account.StudentAccount, 17);
            newAccount.CT_Intitule = StringUtils.CheckLength(account.StudentName,35);
            //TODO newAccount.c = account.OpcaAccount;
            //newAccount.CT_Contact = account.StudentName;
            newAccount.Adresse.Adresse = StringUtils.CheckLength(account.StudentAddress1,35);
            newAccount.Adresse.Complement = StringUtils.CheckLength(account.StudentAddress2,35);
            newAccount.Adresse.CodePostal = StringUtils.CheckLength(account.StudentPostCode,9);
            newAccount.Adresse.Ville = StringUtils.CheckLength(account.StudentCity,35);
            newAccount.Adresse.Pays = StringUtils.CheckLength(account.StudentCountry,35);
            newAccount.CT_Siret = StringUtils.CheckLength(account.CompanySiret,14);
            newAccount.Telecom.Telephone = StringUtils.CheckLength(account.StudentPhone,21);
            newAccount.Telecom.Telecopie = StringUtils.CheckLength(account.StudentFax,21);
            newAccount.Telecom.EMail = StringUtils.CheckLength(account.StudentEmail,69);
            //TODO Catégorie comptable newAccount. = account.

            newAccount.SetDefault();
            newAccount.Write();
            var createdAccount = businessManagementDb.CptaApplication.FactoryClient.ReadNumero(newAccount.CT_Num);

            createdAccount.InfoLibre[Globals.APPCONFIG.OpcaAccountFreeField] = account.OpcaAccount;
            createdAccount.Write();
            //newAccount.Write();
        }


        private static bool AccountsExists(BSCIALApplication100c businessManagementDb, Account account)
        {
            return businessManagementDb.CptaApplication.FactoryTiers.ExistNumero(account.StudentAccount);
        }
    }
}
