﻿namespace ThirdAndQuoteInterface.Model
{
    class Account
    {
        public string StudentAccount { get; set; }
        public string OpcaAccount { get; set; }
        public string CompanyAccount { get; set; }
        public string CompanyName { get; set; }
        public string Type { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyPostCode { get; set; }
        public string CompanyCity { get; set; }
        public string CompanySiret { get; set; }
        public string CompanyCountry { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyDecisionMaker { get; set; }
        public string SocietyKey { get; set; }
        public string OpcaName { get; set; }
        public string OpcaAddress1 { get; set; }
        public string OpcaAddress2 { get; set; }
        public string OpcaPostCode { get; set; }
        public string OpcaCity { get; set; }
        public string OpcaCountry { get; set; }
        public string OpcaPhone { get; set; }
        public string OpcaFax { get; set; }
        public string OpcaEmail { get; set; }
        public string OpcaDecisionMaker { get; set; }
        public string StudentName { get; set; }
        public string StudentAddress1 { get; set; }
        public string StudentAddress2 { get; set; }
        public string StudentPostCode { get; set; }
        public string StudentCity { get; set; }
        public string StudentCountry { get; set; }
        public string StudentPhone { get; set; }
        public string StudentFax { get; set; }
        public string StudentEmail { get; set; }
        public string FinancierType { get; set; }
    }
}
