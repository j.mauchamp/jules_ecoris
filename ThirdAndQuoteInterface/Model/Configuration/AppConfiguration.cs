﻿using System.Configuration;
using ThirdAndQuoteInterface.Utils;

namespace ThirdAndQuoteInterface.Model.Configuration
{
    class AppConfiguration
    {
        public string GescomPath = ConfigurationManager.AppSettings["gescomPaths"];
        public string GescomUser = ConfigurationManager.AppSettings["gescomUser"];
        public string GescomPassword = ConfigurationManager.AppSettings["gescomPassWord"];
        public string RootPath = ConfigurationManager.AppSettings["rootPath"];
        public string FileFormat = ConfigurationManager.AppSettings["fileFormat"];
        public string ArchivePath = ConfigurationManager.AppSettings["archivePath"];
        public string sqlConnectionString =
            $@"Server={ConfigurationManager.AppSettings["sqlServer"]};Database={ConfigurationManager.AppSettings["sqlBase"]};
                    User Id={ConfigurationManager.AppSettings["sqlUser"]};
                    Password={StringUtils.DecodePassword(ConfigurationManager.AppSettings["sqlPasswordCrypt"])};";

        public string OpcaAccountFreeField = ConfigurationManager.AppSettings["opcaAccountFreeField"];
    }
}
