﻿using System;
using ThirdAndQuoteInterface.BusinessLogic;
using ThirdAndQuoteInterface.DAL;
using ThirdAndQuoteInterface.Utils;

namespace ThirdAndQuoteInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            var bussinessManagementDb = Gescom.OpenDB();
            var accounts = Accounts.GetAccounts();
            AccountProcess.CreateAccounts(accounts, bussinessManagementDb);
        }
    }
}
