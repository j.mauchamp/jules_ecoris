﻿using Objets100cLib;

namespace ThirdAndQuoteInterface.Utils
{
    class Gescom
    {
        public static BSCIALApplication100c OpenDB()
        {
            var businessManagementDb = new BSCIALApplication100c();
            try
            {
                businessManagementDb.Name = Globals.APPCONFIG.GescomPath;
                businessManagementDb.Loggable.UserName = Globals.APPCONFIG.GescomUser;
                businessManagementDb.Loggable.UserPwd = Globals.APPCONFIG.GescomPassword;
                businessManagementDb.Open();
            }
            catch
            {
                //todo log
            }
            return businessManagementDb;
        }

    }
}
