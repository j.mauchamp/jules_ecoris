﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdAndQuoteInterface.Utils
{
    class StringUtils
    {
        public static string CheckLength(string value, int length)
        {
            if (value.Length > length)
            {
                return value.Substring(0, length - 1);
            }
            else
            {
                return value;
            }
        }

        public static string GetDateToString()
        {
            return $"{DateTime.Now:yyyyMMddhhmmssffff}";
        }

        public static string DecodePassword(string password)
        {
            var base64EncodedBytes = Convert.FromBase64String(password);
            password = Encoding.UTF8.GetString(base64EncodedBytes);
            return password;
        }

        public static string ConvertDiacritics(string line)
        {
            string decomposed = line.Normalize(NormalizationForm.FormD);
            char[] filtered = decomposed
                .Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                .ToArray();
            string newLine = new String(filtered);
            return newLine;
        }
    }
}
