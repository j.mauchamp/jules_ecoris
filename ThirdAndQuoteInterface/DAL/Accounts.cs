﻿using System;
using System.Collections.Generic;
using System.Linq;
using ThirdAndQuoteInterface.Utils;
using ThirdAndQuoteInterface.Model;

namespace ThirdAndQuoteInterface.DAL
{
    class Accounts
    {
        public static List<Account> GetAccounts()
        {
            //_currentMethod = MethodBase.GetCurrentMethod().Name;

            var accountFiles = FileUtils.GetFiles();
            List<Account> accounts = new List<Account>();

            foreach (var accountFile in accountFiles)
            {
                try
                {
                    //Globals.LOG.AddEntry(new LogEntry(_currentClass, _currentMethod,
                    //    "Récupération des informations d'un compte tier depuis un fichier csv ou txt", "START",
                    //    $"Début de la récupération des informations", String.Empty));

                    GetAccount(accounts, accountFile);
                }
                catch (Exception e)
                {
                    //Globals.LOG.AddEntry(new LogEntry(_currentClass, _currentMethod,
                    //    "Récupération des informations d'un compte tier depuis un fichier csv ou txt", "ERROR",
                    //    $"Message : {e.Message} - Trace : {e.StackTrace}", String.Empty));
                    Console.WriteLine($"Message : {e.Message}  erreur : {e.StackTrace}");
                }
            }
            return accounts;
        }

        private static void GetAccount(List<Account> accounts, System.IO.FileInfo accountFile)
        {
            var accountFileLines = FileUtils.GetFileLines(accountFile.FullName);

            foreach (var accountFileLine in accountFileLines.Where(l => !string.IsNullOrWhiteSpace(l) && !string.IsNullOrEmpty(l) && !l.ToLower().StartsWith("compte etudiant")))
            {
                var line = accountFileLine.Split(';');

                Account account = new Account();

                account.StudentAccount = StringUtils.ConvertDiacritics(line[0]);
                account.OpcaAccount = line[1];
                account.CompanyAccount = line[2];
                account.CompanyName = line[3];
                account.Type = line[4];
                account.CompanyAddress1 = line[5];
                account.CompanyAddress2 = line[6];
                account.CompanyPostCode = line[7];
                account.CompanyCity = line[8];
                account.CompanySiret = line[9];
                account.CompanyCountry = line[10];
                account.CompanyPhone = line[11];
                account.CompanyFax = line[12];
                account.CompanyEmail = line[13];
                account.CompanyDecisionMaker = line[14];
                account.SocietyKey = line[15];
                account.OpcaName = line[16];
                account.OpcaAddress1 = line[17];
                account.OpcaAddress2 = line[18];
                account.OpcaPostCode = line[19];
                account.OpcaCity = line[20];
                account.OpcaCountry = line[21];
                account.OpcaPhone = line[22];
                account.OpcaFax = line[23];
                account.OpcaEmail = line[24];
                account.OpcaDecisionMaker = line[25];
                account.StudentName = line[26];
                account.StudentAddress1 = line[27];
                account.StudentAddress2 = line[28];
                account.StudentPostCode = line[29];
                account.StudentCity = line[30];
                account.StudentCountry = line[31];
                account.StudentPhone = line[32];
                account.StudentFax = line[33];
                account.StudentEmail = line[34];
                account.FinancierType = line[35];

                accounts.Add(account);

                //Globals.LOG.AddEntry(new LogEntry(_currentClass, _currentMethod,
                //    "Récupération des informations d'un client depuis un fichier csv", "SUCCES",
                //    $"FIn de la récupération des informations", String.Empty));

            }
        }
    }
}
